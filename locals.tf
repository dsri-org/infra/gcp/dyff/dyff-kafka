# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment   = "dyff-kafka"
  name         = "${var.environment}-${local.deployment}"
  cluster_name = "${var.environment}-dyff-cloud"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  # make sure all components use same compression.type
  compression_type = "zstd"

  replica_count = 3

  bootstrap_servers = "kafka.kafka.svc.cluster.local:9092" # Port number is mandatory

  topics = [
    {
      name = "dyff.workflows.events"
      config = {
        "compression.type" = local.compression_type

        # retain event records for 7 days
        "retention.bytes" = "-1"
        "retention.ms"    = "604800000"
      }
    },
    {
      # messages in state are expected to be retained forever unless deleted
      name = "dyff.workflows.state"
      config = {
        "compression.type" = local.compression_type

        # retain only the latest value for each key
        "cleanup.policy" = "compact"
        # retain tombstones in the topic for 7 days
        "delete.retention.ms" = "604800000"
        # compact records within 7 days -> enforces hard-delete deadline
        "max.compaction.lag.ms" = "604800000"

        # retain state records forever
        "retention.bytes" = "-1"
        "retention.ms"    = "-1"
      }
    }
  ]

  topics_map = {
    for topic in local.topics : topic.name => topic
  }
}
