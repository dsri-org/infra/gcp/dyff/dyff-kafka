# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "bootstrap_servers" {
  value = module.root.bootstrap_servers
}

output "topics_map" {
  value = module.root.topics_map
}
