# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "bootstrap_servers" {
  value = local.bootstrap_servers
}

output "topics_map" {
  value = local.topics_map
}
