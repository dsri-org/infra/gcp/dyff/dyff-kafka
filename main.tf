# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "random_string" "cluster_id" {
  length  = 32
  special = false
}

resource "kubernetes_namespace" "kafka" {
  metadata {
    name = "kafka"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/bitnami/kafka
resource "helm_release" "kafka" {
  name       = "kafka"
  namespace  = kubernetes_namespace.kafka.metadata[0].name
  repository = "oci://registry-1.docker.io/bitnamicharts"
  version    = "27.1.2"
  chart      = "kafka"

  skip_crds = true

  timeout = 600

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    controller = {
      affinity = {
        podAntiAffinity = {
          requiredDuringSchedulingIgnoredDuringExecution = [{
            labelSelector = {
              matchLabels = {
                "app.kubernetes.io/component" = "controller-eligible"
                "app.kubernetes.io/instance"  = "kafka"
                "app.kubernetes.io/name"      = "kafka"
              }
            }
            topologyKey = "topology.kubernetes.io/zone"
          }]
        }
      }

      # https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/volume-expansion
      persistence = {
        enabled = true

        # Storage size cannot be changed in a StatefulSet after creation, but
        # the PVCs themselves can be manually resized. We would benefit from a
        # script to make this resizing easier.
        size = "10Gi"

        # This storage class supports allowVolumeExpansion.
        storageClass = "retain-standard-rwo"
      }

      replicaCount = local.replica_count

      resourcesPreset = "large"
    }

    extraConfig = {
      autoCreateTopicsEnable = false
    }

    kraft = {
      clusterId = random_string.cluster_id.result
    }

    listeners = {
      client = {
        protocol = "PLAINTEXT"
      }
    }

    metrics = {
      kafka = {
        enabled = true
      }
    }

    persistentVolumeClaimRetentionPolicy = {
      enabled     = true
      whenScaled  = "Retain"
      whenDeleted = "Retain"
    }

    provisioning = {
      enabled           = true
      numPartitions     = 60
      replicationFactor = 3
      resourcesPreset   = "small"
      topics            = local.topics

      # the wait-for-kafka init container hard-codes the timeout, so we get to
      # painstakingly reproduce the entire manifest section to change it
      waitForKafka = false

      initContainers = [{
        name            = "wait-for-available-kafka"
        image           = "docker.io/bitnami/kafka:3.7.0-debian-12-r0"
        imagePullPolicy = "IfNotPresent"
        securityContext = {
          allowPrivilegeEscalation = false
          capabilities = {
            drop = ["ALL"]
          }
          readOnlyRootFilesystem = true
          runAsGroup             = 1001
          runAsNonRoot           = true
          runAsUser              = 1001
        }
        command = ["/bin/bash"]
        args = [
          "-ec",
          <<-EOF
          sleep 300;
          wait-for-port --host=kafka --state=inuse --timeout=600 9092;
          echo "Kafka is available";
          EOF
        ]
        resources = {
          limits = {
            "cpu"               = "750m"
            "ephemeral-storage" = "1024Mi"
            "memory"            = "768Mi"
          }
          requests = {
            "cpu"               = "500m"
            "ephemeral-storage" = "50Mi"
            "memory"            = "512Mi"
          }
        }
      }]
    }
  })]
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
# https://cloud.google.com/stackdriver/docs/managed-prometheus/setup-managed#gmp-servicemonitor
resource "kubernetes_manifest" "pod_monitoring" {
  manifest = {
    apiVersion = "monitoring.googleapis.com/v1"
    kind       = "PodMonitoring"
    metadata = {
      name      = "kafka-metrics"
      namespace = kubernetes_namespace.kafka.metadata[0].name
    }
    spec = {
      selector = {
        matchLabels = {
          "app.kubernetes.io/instance"  = "kafka"
          "app.kubernetes.io/name"      = "kafka"
          "app.kubernetes.io/component" = "cluster-metrics"
        }
      }
      endpoints = [{
        port = "metrics"
        path = "/metrics"
      }],
    }
  }
}
