#!/bin/bash
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

kubectl exec -it -n kafka sts/kafka-controller -c kafka -- /opt/bitnami/kafka/bin/kafka-topics.sh \
    --describe \
    --bootstrap-server kafka.kafka.svc.cluster.local:9092
