#!/bin/bash
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

kubectl rollout restart -n dyff-api deploy dyff-api
kubectl rollout restart -n dyff-orchestrator sts dyff-orchestrator
kubectl rollout restart -n dyff-operator deploy dyff-operator
kubectl rollout restart -n workflows-aggregator sts workflows-aggregator
kubectl rollout restart -n workflows-informer deploy workflows-informer
kubectl rollout restart -n workflows-sink deploy workflows-sink-mongodb
kubectl rollout restart -n workflows-sink deploy workflows-sink-backup-s3
