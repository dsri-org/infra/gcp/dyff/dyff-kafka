#!/bin/bash
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# don't stop on error so that script can be re-run
set -uo pipefail

# uninstall the kafka chart
helm uninstall kafka -n kafka

# delete the PVCs
kubectl delete pvc -n kafka data-kafka-controller-{0..2}

# delete the retained PVs corresponding to the PVCs
kubectl get pv | grep kafka/data-kafka-controller | awk '{print $1}' | xargs kubectl delete pv
